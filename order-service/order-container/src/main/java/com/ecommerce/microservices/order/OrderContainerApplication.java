package com.ecommerce.microservices.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.ecommerce.microservices.order")
@EntityScan(basePackages = "com.ecommerce.microservices.order")
public class OrderContainerApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderContainerApplication.class, args);
    }

    @Bean
    public OrderDomainService orderDomainService(){
        return new OrderDomainServiceImpl();
    }
}