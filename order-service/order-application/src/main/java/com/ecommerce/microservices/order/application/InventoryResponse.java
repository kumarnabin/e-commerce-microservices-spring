package com.ecommerce.microservices.order.application;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class InventoryResponse {
    private String skuCode;
    private boolean isInStock;
}
