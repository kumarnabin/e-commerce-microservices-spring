package com.ecommerce.microservices.order;

import com.ecommerce.microservices.order.model.Order;


public class OrderDomainServiceImpl  implements OrderDomainService{

    @Override
    public void initializeOrder(Order order) {
        order.validateOrder();
        order.initializeOrder();
        // TODO return some event
    }

    @Override
    public void payOrder(Order order) {
        order.pay();
    }

    @Override
    public void approveOrder(Order order) {

    }

    @Override
    public void cancelOrder(Order order) {

    }
}
