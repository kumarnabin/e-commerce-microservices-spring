package com.ecommerce.microservices.order.valueobject;

public enum OrderStatus {
    PENDING, PAID, CANCELLED
}
