package com.ecommerce.microservices.order.valueobject;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@AllArgsConstructor
@Getter
@Setter
public class OrderItemId {
    private final Long id;
}
