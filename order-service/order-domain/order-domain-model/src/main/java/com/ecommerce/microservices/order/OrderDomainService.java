package com.ecommerce.microservices.order;


import com.ecommerce.microservices.order.model.Order;

public interface OrderDomainService {
    void initializeOrder(Order order);
    void payOrder(Order order);
    void approveOrder(Order order);
    void cancelOrder(Order order);
}
