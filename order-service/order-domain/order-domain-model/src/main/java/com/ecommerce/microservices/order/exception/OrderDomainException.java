package com.ecommerce.microservices.order.exception;

public class OrderDomainException extends RuntimeException {
    public OrderDomainException(String message){
        super(message);
    }
}
