package com.ecommerce.microservices.order.ports;

import com.ecommerce.microservices.order.dto.CreateOrderCommand;
import com.ecommerce.microservices.order.dto.OrderResponse;
import com.ecommerce.microservices.order.dto.CreateOrderResponse;

import java.util.List;

public interface OrderAppService {
    CreateOrderResponse createOrder(CreateOrderCommand createOrderCommand);
    List<OrderResponse> getAllOrders();
}
