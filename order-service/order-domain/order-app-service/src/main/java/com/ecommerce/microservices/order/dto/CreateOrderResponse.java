package com.ecommerce.microservices.order.dto;

import com.ecommerce.microservices.order.valueobject.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
public class CreateOrderResponse {
    private final UUID parcelId;
    private final OrderStatus orderStatus;
}
