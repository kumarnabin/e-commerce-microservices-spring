package com.ecommerce.microservices.order.ports;


import com.ecommerce.microservices.order.model.Order;

import java.util.List;

public interface OrderPersist {
    Order save(Order order);
    List<Order> findAll();
}
