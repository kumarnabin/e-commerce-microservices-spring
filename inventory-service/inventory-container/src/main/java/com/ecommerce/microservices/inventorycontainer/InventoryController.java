package com.ecommerce.microservices.inventorycontainer;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/inventory")
public class InventoryController {

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<InventoryResponse> isInStock(@RequestParam List<String> skuCode){
        System.out.println(InventoryResponse.builder().isInStock(true).skuCode("4354").build());
        System.out.println("=================================");
        System.out.println("=================================");
        System.out.println("=================================");
        System.out.println(skuCode);

        return Arrays.asList(InventoryResponse.builder().isInStock(true).skuCode("4354").build());
    }

}
