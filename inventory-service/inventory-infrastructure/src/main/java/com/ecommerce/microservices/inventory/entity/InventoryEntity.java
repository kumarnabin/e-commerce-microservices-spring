package com.ecommerce.microservices.inventory.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "inventory")
public class InventoryEntity {
    @Id
    private Long id;
    private String skuCode;
    private Long quantity;
}

