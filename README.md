### App Flow

![img.png](img.png)

### Module graph generation

![alt text](dependency-graph.png)

`mvn com.github.ferstl:depgraph-maven-plugin:aggregate -DcreateImage=true -DreduceEdges=false -Dscope=compile "-Dincludes=com.ecommerce.microservices*:*"`


`GET http://localhost:9090/orders`

```json
[
  {
      "id": "c947cf58-1a83-4cb7-8e21-0c8d7fcd951c",
      "parcelId": "0c0f4656-bf53-45b6-8fd7-3a4e63242466",
      "price": 1000.00,
      "orderItems": [
        {
          "orderId": null,
          "orderItemId": {
            "id": 1
          },
          "productId": null,
          "price": {
            "amount": 1000.00
          },
          "quantity": 23
        }
      ]
    },
    {
    "id": "7f4e945d-8dc9-41b2-ab5f-495135df20c1",
    "parcelId": "9c2b0650-f68d-48f8-9c4e-9f26cb4a9168",
    "price": 324.00,
    "orderItems": [
    {
    "orderId": null,
    "orderItemId": {
    "id": 1
    },
    "productId": null,
    "price": {
    "amount": 33.00
    },
    "quantity": 55
    }
    ]
  }
]
```

`POST http://localhost:9090/orders`

```json
{
    "price": 1000,
    "items": [
        {
          "productId" : "c6f1b01e-2f33-45e5-8af3-e62cdb8e04d7",
          "quantity": 23,
          "price": 1000,
          "subTotal": 4000
        }
    ]
}
```