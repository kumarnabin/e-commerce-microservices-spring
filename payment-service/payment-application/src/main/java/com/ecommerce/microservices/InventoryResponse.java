package com.ecommerce.microservices;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class InventoryResponse {
    private String skuCode;
    private boolean isInStock;
}
